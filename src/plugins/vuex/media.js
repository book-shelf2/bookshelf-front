import axios from "./axios";

export default {
    actions: {
        pushFile(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('http://localhost:8000/api/media_objects', data,{
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log('rasm jonatildi')
                        console.log(response.data['@id'])
                        console.log(response.data)
                        context.commit('updateMedia', response.data)

                        resolve()
                    })
                    .catch(() => {
                        console.log('rasmlar olishda xatolik')
                        reject()
                    })
            })
        },
    },
    mutations: {
        updateMedia(state, media) {
            state.media = media['@id']
        },
    },
    state: {
        media: {
            id: ''
        }
    },
    getters: {
        getMedia(state) {
            return state.media
        },
    }
}