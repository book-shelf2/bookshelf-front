import {createStore} from "vuex";
import book from "@/plugins/vuex/book";
import category from "@/plugins/vuex/category";
import user from "@/plugins/vuex/user";
import picture from "@/plugins/vuex/picture";
import media from "@/plugins/vuex/media";

export default createStore({
    modules: {
        book,
        category,
        media,
        picture,
        user
    }
})