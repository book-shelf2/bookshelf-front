import axios from "./axios";

export default {
    actions: {
        fetchBooks(context, data) {
            if (!data.page) {
                data.page = 1
            }

            let url = '?page=' +data.page

            if (data.categoryId !== null) {
                url += '&category=' + data.categoryId
            }
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8000/api/books' + url)
                    .then((response) => {
                        console.log('kitoblar muvaffaqiyatli olindi')

                        let books = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }
                        context.commit('updateBooks', books)
                        resolve()
                    })
                    .catch(() => {
                        console.log('rasmlar olishda xatolik')
                        reject()
                    })
            })
        },
        fetchBook(context, bookId) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8000/api/books/' + bookId)
                    .then((response) => {
                        context.commit('updateBook', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('kitoblar olishda xatolik')
                        reject()
                    })
            })
        },
        pushBook(context, data) {
            return new Promise((resolve) => {
                axios
                    .post('http://localhost:8000/api/books/create', data)
                    .then((response) => {
                        console.log('muvaffaqaiyatli, then() ishladi')
                        console.log(response.data)
                        resolve()
                    })
            })
        },
    },
    mutations: {
        updateBooks(state, books) {
            state.books = books
        },
        updateBook(state, book) {
            state.book = book
        }
    },
    state: {
        books: {
            models: [],
            totalItems: 0
        },
        book: {
            name: '',
            text: ''
        }
    },
    getters: {
        getBooks(state) {
            return state.books.models
        },
        getBook(state) {
            return state.book
        },
        getTotalBooks(state) {
            return state.books.totalItems
        }
    }
}