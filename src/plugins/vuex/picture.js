import axios from "./axios";

export default {
    actions: {
        fetchPictures(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8000/api/media_objects')
                    .then((response) => {
                        console.log('rasmlar muvaffaqiyatli olindi')

                        let pictures = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }
                        context.commit('updatePictures', pictures)
                        resolve()
                    })
                    .catch(() => {
                        console.log('rasmlar olishda xatolik')
                        reject()
                    })
            })
        },
    },
    mutations: {
        updatePictures(state, pictures) {
            state.pictures = pictures
        }
    },
    state: {
        pictures: {
            models: [],
            totalItems: 0
        }
    },
    getters: {
        getPictures(state) {
            return state.pictures.models
        }
    }
}